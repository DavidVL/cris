package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Researcher;

public interface ResearcherDAO {
	
	public Researcher create( Researcher Researcher );
	public Researcher read( String ResearcherId );
	public Researcher update( Researcher Researcher );
	public Researcher delete( Researcher Researcher );

	public List<Researcher> readAll();
	public Researcher readByEmail(String email);

}
